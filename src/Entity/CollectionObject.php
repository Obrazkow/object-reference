<?php


namespace ObjectReference\Entity;


use Countable;
use Exception;
use Iterator;

class CollectionObject extends AbstractCollection implements Countable, Iterator
{

    protected $entityIdField;

    public function __construct(string $entityIdField, array $items = [])
    {
        $this->entityIdField = $entityIdField;
        $this->setItems($items);
    }

    /**
     * @param Entity $item
     * @throws Exception
     */
    public function addItem(Entity $item)
    {
        if (!isset($item->data[$this->entityIdField])) {
            throw new \Exception(sprintf('Field %s not exists in entity', $this->entityIdField));
        }

        $entityKey = $item->data[$this->entityIdField];
        $this->items[$entityKey] = $item;
        $this->data[$entityKey] = &$item->data;
    }

    /**
     * @param string $code
     */
    public function removeItem(string $code)
    {
        unset($this->items[$code]);
        unset($this->data[$code]);
    }

    public function setItems(array $items)
    {
        $this->items = $items;
        $newData = [];
        foreach ($items as $item) {
            $newData[$item->data[$this->entityIdField]] = &$item->data;
        }
        $this->data = $newData;
    }

    public function getItem(string $key)
    {
        return $this->items[$key] ?? null;
    }


}