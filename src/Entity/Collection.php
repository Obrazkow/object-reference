<?php


namespace ObjectReference\Entity;


use Countable;
use Iterator;

class Collection extends AbstractCollection implements Countable, Iterator
{

    public function __construct(array $items = [])
    {
        $this->setItems($items);
    }

    public function setItems(array $items)
    {
        $this->items = $items;

        $newData = [];
        foreach ($items as $item) {
            $newData[] = &$item->data;
        }

        $this->data = $newData;
    }

    public function addItem(Entity $item)
    {
        $this->items[] = $item;
        $this->data[] = &$item->data;
    }


}