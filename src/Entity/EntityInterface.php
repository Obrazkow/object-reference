<?php


namespace ObjectReference\Entity;


interface EntityInterface
{

    public function hasValue($key);

    public function setValues(array $data);

    public function getValues(): array;


}