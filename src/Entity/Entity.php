<?php


namespace ObjectReference\Entity;


class Entity extends EntityData implements EntityInterface
{

    public static function init(array $data): self
    {
        $object = new static();
        $object->data = $data;

        return $object;
    }

    public function hasValue($key)
    {
        return array_key_exists($key, $this->data);
    }

    public function setValues(array $data)
    {
        return $this->data = $data;
    }

    public function unsetField($key)
    {
        unset($this->data[$key]);
    }

    public function getValues(): array
    {
        return $this->data;
    }

    protected function setObject($key, EntityData $object)
    {
        $this->setField($key, $object->data);
    }

    protected function setField($key, $value): Entity
    {
        $this->data[$key] = $value;

        return $this;
    }

    protected function getField($key)
    {
        return $this->data[$key] ?? null;
    }

    protected static function initChild(Entity $entity, $path): ?self
    {
        $object = new static();
        $object->data = &$entity->data[$path];

        return $object;
    }

    protected static function initCollection(Entity $entity, string $key): Collection
    {
        $collection = new Collection(static::getListObject($entity, $key));
        $collection->data = &$entity->data[$key];

        return $collection;
    }

    protected static function initObjectCollection(Entity $entity, string $key, string $entityIdField): CollectionObject
    {
        $collection = new CollectionObject($entityIdField, static::getListObject($entity, $key));
        $collection->data = &$entity->data[$key];

        return $collection;
    }

    /**
     * @param Entity $entity
     * @param string $key
     * @return array
     */
    protected static function getListObject(Entity $entity, string $key): array
    {
        $items = [];
        if (isset($entity->data[$key])) {
            foreach ($entity->data[$key] as $k => &$item) {
                $object = new static();
                $object->data = &$item;

                $items[$k] = $object;
            }
        }

        return $items;
    }


}