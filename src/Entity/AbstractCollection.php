<?php


namespace ObjectReference\Entity;


abstract class AbstractCollection extends EntityData
{

    /** @var Entity[] */
    protected $items = [];

    public function getItems()
    {
        return $this->items;
    }

    public function count()
    {
        return count($this->items);
    }

    public function current()
    {
        return current($this->items);
    }

    public function next()
    {
        next($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function valid()
    {
        return key($this->items) !== null;
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function map(callable $func)
    {
        $data = [];
        foreach ($this as $item) {
            $data[] = $func($item);
        }

        return $data;
    }

}